#!/bin/sh
set -e

echo "127.0.0.1 openjabnab.cormeilles.lallemand.fr" >> /etc/hosts

apachectl start

cd /opt/OpenJabNab/server/bin
exec ./openjabnab
